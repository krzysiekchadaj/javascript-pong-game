(function() {

    const DEFAULT_CANVAS_WIDTH = 800; // [px]
    const DEFAULT_CANVAS_HEIGHT = 600; // [px]

    const WINNING_SCORE = 3;
    const FRAMES_PER_SECOND = 30;
    
    const SPEED_CHANGE_COEFICIENT = 0.35;
    const PAUSE_LENGTH = 500; // [ms]
    
    const INIT_PADDLE_HEIGHT = 100; // [px]
    const INIT_COMPUTER_PADDLE_SPEED = 12; // [px/s]
    const PADDLE_THICKNESS = 15; // px
    const PADDLE_OFFSET = 2; // px

    const INIT_BALL_X = 50; // [px]
    const INIT_BALL_Y = 50; // [px]
    const INIT_BALL_SPEED_X = 10; // [px/s]
    const INIT_BALL_SPEED_Y = 4; // [px/s]
    const INIT_BALL_RADIUS = 8;

    const STATE_START = 1;
    const STATE_GAME = 2;
    const STATE_WIN = 3;    

    var paddleHeight = INIT_PADDLE_HEIGHT;
    var computerPaddleSpeed = INIT_COMPUTER_PADDLE_SPEED;

    var leftPlayerScore = 0;
    var rightPlayerScore = 0;

    var leftPaddleY = 250;
    var rightPaddleY = 250;

    var canvas;
    var context;
    var interval;
    var state;

    var ballMissed = false;

    var ball = {
        x: INIT_BALL_X,
        y: INIT_BALL_Y,
        speed: {
            x: INIT_BALL_SPEED_X,
            y: INIT_BALL_SPEED_Y
        },
        radius: INIT_BALL_RADIUS,
        color: 'white',
    };

    var text = {
        font: '15px Arial',
        color: 'yellow',
        align: 'center',
        titleFont: '30px Arial'
    }

    window.onload = function() {
        canvas = document.getElementById('gameCanvas');
        context = canvas.getContext('2d');
        window.addEventListener('resize', resizeCanvas, false);
        resizeCanvas();
        start();
    }

    function resizeCanvas() {
        previousWidth = canvas.width;
        previousHeight = canvas.height;

        canvas.width = window.innerWidth;
        canvas.height = window.innerHeight;

        recalculateVariables(previousWidth, previousHeight);
        drawScreen();
    }

    function recalculateVariables(previousWidth, previousHeight) {
        var multiplierHeight = canvas.height / DEFAULT_CANVAS_HEIGHT;
        var multiplierWidth = canvas.width / DEFAULT_CANVAS_WIDTH;

        var multiplierMin = Math.min(multiplierHeight, multiplierWidth);
        var multiplierMax = Math.max(multiplierHeight, multiplierWidth);

        var coefficientWidth = ( canvas.width / previousWidth );
        var coefficientHeight = ( canvas.height / previousHeight );

        recalculatePaddlesY(coefficientHeight);
        recalculatePaddleHeight(multiplierHeight);
        recalculateCompPaddleSpeed(multiplierMax)

        recalculateBallPosition(coefficientWidth, coefficientHeight);
        recalculateBallSpeed(multiplierWidth, multiplierHeight);        
        recalculateBallRadius(multiplierMin);
    }

    function recalculatePaddlesY(coefficientHeight) {
        leftPaddleY *= coefficientHeight;
        rightPaddleY *= coefficientHeight;
    }

    function recalculatePaddleHeight(multiplierHeight) {
        paddleHeight = INIT_PADDLE_HEIGHT * multiplierHeight;
    }

    function recalculateCompPaddleSpeed(multiplierMax) {
        computerPaddleSpeed = INIT_COMPUTER_PADDLE_SPEED * multiplierMax;
    }

    function recalculateBallPosition(coefficientWidth, coefficientHeight) {
        ball.x *= coefficientWidth;
        ball.y *= coefficientHeight;
    }

    function recalculateBallSpeed(multiplierWidth, multiplierHeight) {
        ball.speed.x = INIT_BALL_SPEED_X * multiplierWidth;
        ball.speed.y = INIT_BALL_SPEED_Y * multiplierHeight;
        
    }

    function recalculateBallRadius(multiplierMin) {
        ball.radius = INIT_BALL_RADIUS * multiplierMin;
    }

    function drawScreen() {
        if (state === STATE_START) {
            drawStartScreen();
            return;
        }
        if (state === STATE_GAME) {
            draw();
            return;
        }
        if (state === STATE_WIN) {
            drawWinScreen();
            return;
        }
    }

    function start() {
        drawStartScreen();
        canvas.addEventListener('mousedown', startGame);
        state = STATE_START;
    }

    function startGame() {
        canvas.removeEventListener('mousedown', startGame);
        canvas.addEventListener('mousemove', moveLeftPaddle);
        interval = setInterval(frame, 1000/FRAMES_PER_SECOND);
        state = STATE_GAME;
    }

    function moveLeftPaddle(evt) {
        var mousePos = calculateMousePosition(evt);
        leftPaddleY = mousePos.y - paddleHeight / 2;
    }

    function calculateMousePosition(evt) {
        var root = document.documentElement;
        var rectangle = canvas.getBoundingClientRect();
        var mouseX = evt.clientX - rectangle.left - root.scrollLeft;
        var mouseY = evt.clientY - rectangle.top - root.scrollTop;
        return {
            x: mouseX,
            y: mouseY
        }
    }

    function frame() {
        move();
        draw();
        checkWinningCondition();
    }

    function move() {
        updateBallPosition();
        computerMovement();
        checkCollisionTop();
        checkCollisionBottom();
        checkCollisionLeft();
        checkCollisionRight();
        saturateBallPosition();
    }

    function updateBallPosition() {
        ball.x += ball.speed.x;
        ball.y += ball.speed.y;
    }

    function computerMovement() {
        var paddle2Ycenter = rightPaddleY + paddleHeight / 2;
        if (paddle2Ycenter < ball.y - paddleHeight / 3) {
            rightPaddleY += computerPaddleSpeed;
        } else if (paddle2Ycenter > ball.y + paddleHeight / 3) {
            rightPaddleY -= computerPaddleSpeed;
        }
    }

    function checkCollisionTop() {
        if (ball.y < ball.radius) {
            ball.speed.y = -ball.speed.y;
        }
    }

    function checkCollisionBottom() {
        if (ball.y > canvas.height - ball.radius) {
            ball.speed.y = -ball.speed.y;
        }
    }

    function checkCollisionLeft() {
        if (ball.x < -ball.radius) {
            rightPlayerScore++;
            pause();
            resetBall();
        } else if (ball.x < PADDLE_THICKNESS + PADDLE_OFFSET + ball.radius) {
            if ((!ballMissed) && (ball.y > leftPaddleY && ball.y < leftPaddleY + paddleHeight)) {
                bounceBall(leftPaddleY);
            } else {
                ballMissed = true;
            }
        }
    }

    function checkCollisionRight() {
        if (ball.x > canvas.width + ball.radius) {
            leftPlayerScore++;
            pause();
            resetBall();
        } else if (ball.x > canvas.width - PADDLE_THICKNESS - PADDLE_OFFSET - ball.radius) {
            if ((!ballMissed) && (ball.y > rightPaddleY && ball.y < rightPaddleY + paddleHeight)) {
                bounceBall(rightPaddleY);
            } else {
                ballMissed = true;
            }
        }
    }

    function saturateBallPosition() {
        saturateBallTop();
        saturateBallBottom();
    }

    function saturateBallTop() {
        if (ball.y < ball.radius) {
            ball.y = ball.radius;
        }
    }

    function saturateBallBottom() {
        if (ball.y > canvas.height - ball.radius) {
            ball.y = canvas.height - ball.radius;
        }
    }

    function bounceBall(paddleY) {
        ball.speed.x = -ball.speed.x;
        var deltaY = ball.y - paddleY - paddleHeight / 2;
        ball.speed.y = SPEED_CHANGE_COEFICIENT * deltaY;
    }

    function pause() {
        clearInterval(interval);
        setTimeout(function() {
            interval = setInterval(frame, 1000/FRAMES_PER_SECOND);
        }, PAUSE_LENGTH);
    }

    function resetBall() {
        ball.speed.x = -ball.speed.x;
        ball.x = canvas.width / 2;
        ball.y = canvas.height / 2;
        ballMissed = false;
    }

    function checkWinningCondition() {
        if (leftPlayerScore >= WINNING_SCORE || rightPlayerScore >= WINNING_SCORE) {
            win();
        }
    }

    function win() {
        canvas.removeEventListener('mousemove', moveLeftPaddle);
        clearInterval(interval);
        drawWinScreen();
        canvas.addEventListener('mousedown', restartGame);
        state = STATE_WIN;
    }

    function restartGame(evt) {
        canvas.removeEventListener('mousedown', restartGame);
        leftPlayerScore = 0;
        rightPlayerScore = 0;
        initBallState();    
        startGame();
    }

    function initBallState() {
        ball.x = INIT_BALL_X;
        ball.y = INIT_BALL_Y;
        ball.speed.x = INIT_BALL_SPEED_X;
        ball.speed.y = INIT_BALL_SPEED_Y;
    }

    function drawRectangle(x, y, width, height, color) {
        context.fillStyle = color;
        context.fillRect(x, y, width, height, color);
    }

    function drawBackground() {
        drawRectangle(0, 0, canvas.width, canvas.height, 'black');
    }

    function drawNet() {
        const netSegments = 15;
        var netSegmentLength = canvas.height/netSegments;
        for (var i = 0.25*netSegmentLength; i < canvas.height; i+=netSegmentLength) {
            drawRectangle(canvas.width / 2 - 1, i, 2, 0.5*netSegmentLength, 'white');
        }
    }

    function drawLeftPaddle() {
        drawRectangle(PADDLE_OFFSET, leftPaddleY, PADDLE_THICKNESS, paddleHeight, 'white');
    }

    function drawRightPaddle() {
        drawRectangle(canvas.width - PADDLE_THICKNESS - PADDLE_OFFSET, rightPaddleY, PADDLE_THICKNESS, paddleHeight, 'white');
    }

    function drawCircle(centerX, centerY, radius, color) {
        context.fillStyle = color;
        context.beginPath();
        context.arc(centerX, centerY, radius, 0, 2*Math.PI, true);
        context.fill();
    }

    function drawBall() {
        drawCircle(ball.x, ball.y, ball.radius, ball.color);
    }

    function drawLeftScore() {
        context.fillStyle = text.color;
        context.font = text.font;
        context.textAlign = text.align;
        context.fillText(leftPlayerScore, 0.45*canvas.width, 0.1*canvas.height);
    }

    function drawRightScore() {
        context.fillStyle = text.color;
        context.font = text.font;
        context.textAlign = text.align;
        context.fillText(rightPlayerScore, 0.55*canvas.width, 0.1*canvas.height);
    }

    function drawScore() {
        drawLeftScore();
        drawRightScore();
    }

    function draw() {
        drawBackground();
        drawNet();
        drawLeftPaddle();
        drawRightPaddle();
        drawBall();
        drawScore();
    }

    function leftWinMessage() {
        context.fillStyle = text.color;
        context.font = text.font;
        context.textAlign = text.align;
        context.fillText("Left Player Won!", 0.5*canvas.width, 0.5*canvas.height);
    }

    function rightWinMessage() {
        context.fillStyle = text.color;
        context.font = text.font;
        context.textAlign = text.align;
        context.fillText("Right Player Won!", 0.5*canvas.width, 0.5*canvas.height);
    }

    function continueMessage() {
        context.fillStyle = text.color;
        context.font = text.font;
        context.textAlign = text.align;
        context.fillText("Click to continue...", 0.5*canvas.width, 0.875*canvas.height);
    }

    function drawWinScreen() {
        drawBackground();
        if (leftPlayerScore >= WINNING_SCORE) {
            leftWinMessage();    
        }
        if (rightPlayerScore >= WINNING_SCORE) {
            rightWinMessage();
        }
        drawScore();
        continueMessage();
    }

    function titleMessage() {
        context.fillStyle = text.color;
        context.font = text.titleFont;
        context.textAlign = text.align;
        context.fillText("PONG GAME", 0.5*canvas.width, 0.33*canvas.height);
    }

    function controlMessage() {
        context.fillStyle = text.color;
        context.font = text.font;
        context.textAlign = text.align;
        context.fillText("Control left paddle with the mouse. First three points wins.", 0.5*canvas.width, 0.66*canvas.height);
    }

    function drawStartScreen() {
        drawBackground();
        titleMessage();
        controlMessage();
        continueMessage();
    }

})();
